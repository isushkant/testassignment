import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class LocationsService {
  constructor(private http: HttpClient) {}

  /**
   * HTTP service call to fetch the locations and filter the locations based on active status
   * @return {Observable}
   */
  getLocations(): Observable<any> {
    return this.http.get('https://api.megaport.com/v2/locations')
      .pipe(
        map((res: any) => res.data.filter((value: any) => value.status === 'Active')),
        catchError(err => of('error', err))
      )

  }
}
