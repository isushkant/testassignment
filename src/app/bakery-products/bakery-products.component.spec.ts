import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BakeryProductsComponent } from './bakery-products.component';
import { LocationsService } from 'src/app/services/locations.service';


describe('BakeryProductsComponent', () => {
  let component: BakeryProductsComponent;
  let fixture: ComponentFixture<BakeryProductsComponent>;
  const mockLocationsService = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BakeryProductsComponent ],
      providers: [
        { provide: LocationsService, useValue: mockLocationsService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BakeryProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
