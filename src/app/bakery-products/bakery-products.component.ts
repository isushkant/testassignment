import { Component, OnInit } from '@angular/core';
import { BakeryModel } from './bakery-products.model';
import { BakeryService } from 'src/app/services/bakery.service';
import { LocationsService } from 'src/app/services/locations.service';

@Component({
  selector: 'app-bakery-products',
  templateUrl: './bakery-products.component.html',
  styleUrls: ['./bakery-products.component.css']
})
export class BakeryProductsComponent implements OnInit {
  displaypathValue = ''
  sortType: string = '';
  showListTemplate: boolean = true;
  bakeryItem: BakeryModel = {
    id: 1,
    type: '',
    name: '',
    topping: ''
  }
  finalBakeryList: BakeryModel[] = [];
  bakeryItems: BakeryModel[] = [];

  assignment2 ={
    name: 'Megaport',
    address: {
      office: {
        unit: 'Level 3',
        street: '825 Ann Street',
        suburb: 'Fortitude Valley',
        city: 'Brisbane',
        state: 'Queensland',
        postcode: 4006,
      },
    },
    industry: {
      type: 'Internet and telecommunications',
      asxListed: true,
    },
  };

  constructor(private bakeryService: BakeryService,
              private locationsService: LocationsService) { }

  ngOnInit(): void {
    this.bakeryItems = this.bakeryService.getBakeryItems();
    this.finalBakeryList = this.bakeryItems;
  }

  /**
   * Sort the table by Id
   * @param {String} order - basis for sorting like ascending or descending
   */
  sortById(order: string){
    this.sortType = `id${order}`;
    order == 'asc' ? this.finalBakeryList.sort((a,b) => a.id - b.id):
      this.finalBakeryList.sort((a,b) => b.id - a.id);
  }

  /**
   * Sort the table based on order and table column
   * @param {String} order - basis for sorting like ascending or descending
   * @param {String} type - table column which needs to be sorted
   */
  sortByType(order: string, type: string){
    let valueA;
    let valueB;
    this.sortType = `${type}${order}`;
      this.finalBakeryList.sort(function(a: any, b: any) {
        valueA = a[type].toUpperCase(); // ignore upper and lowercase
        valueB = b[type].toUpperCase(); // ignore upper and lowercase

        if (valueA < valueB) {
          return order == 'asc' ? -1 : 1;
        }
        if (valueA > valueB) {
          return order == 'asc' ? 1 : -1;
        }

        // values must be equal
        return 0;
    })
  }

  /**
   * Filter the table  results based on input. Filtering is  supported on type, name and topping columns of the table
   * @param {String} query - query string used for filtering the records
   */
  filterBakeryItems(query: string){
    this.finalBakeryList = this.bakeryItems.filter(el => el.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
                          || el.type.toLowerCase().indexOf(query.toLowerCase()) !== -1
                          || el.topping.toLowerCase().indexOf(query.toLowerCase()) !== -1)
  }

  /**
   * Toggle to form view from table view
   */
  addNewItem() {
    this.showListTemplate = false;
  }

  /**
   * Submit the form for adding new item and return back to the list view
   * @param {Form} form - form data
   */
  onSubmit(form: any) {
    if(form.valid) {
      const formValue = form.value;
      this.finalBakeryList.push({
        id: formValue.itemId,
        type: formValue.itemType,
        name: formValue.itemName,
        topping: formValue.itemTopping
      });
      this.showListTemplate = true;
      form.reset();
      alert('Item added successfully');
    }
  }

  goToListView() {
    this.showListTemplate = true;
  }

  /**
   * Activity 2
   * get the value of the property
   * @param {String} path - the object path ie. 'address.office.state'
   * @return {String} returns the value of the path passed
   */
  getValue(path : string) {
    let pathArr = path.split('.');
    this.displaypathValue = pathArr.reduce((prev: any, curr: any) => prev && prev[curr], this.assignment2);
  }

  /**
   * Activity 3
   * get list of all active locations
   * for the assignment I have printed res in the console
   */
  getLocations() {
   this.locationsService.getLocations()
     .subscribe(
       res => console.log(res),
       err => console.log('HTTP Error', err),
     )
  }
}


