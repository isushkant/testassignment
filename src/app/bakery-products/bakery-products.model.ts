export interface BakeryModel {
    id: number;
    type: string;
    name: string;
    topping: string;
}