# Bakery

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.4.

All three Activities have been done in the same component.
1. Activity 1: I have created the component bakery-products to displat the list of all the bakery items, sort, filter and add new items.
2. Activity 2: Added a function called `getValue` in `src/app/bakery-products/bakery-products.component.ts` where it would expect path and object as input parameters and return the value. 
3. Activity 3: Added a function called `getLocations` in `src/app/bakery-products/bakery-products.component.ts` where it would make a service call to fetch the filtered records from API endpoint.


## Development server
To run the project locally, install angular cli globally using the following command:
npm install -g @angular/cli

Install all the npm packages using the following command:
npm i

Run the project:
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

